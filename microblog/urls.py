from django.conf.urls import patterns, url
from django.views.generic import TemplateView
from django.views.generic import ListView

from models import Wpis
import views


urlpatterns = patterns('',
        url(r'^$', views.index),
        url(r'^dodaj/', views.dodaj),
        url(r'^add/', views.add, name='add'),
        url(r'^login/', views.login, name='login'),
        url(r'^doLogin/', views.doLogin, name='doLogin'),
        url(r'^logout/', views.logout, name='logout'),
        url(r'^register/', views.register, name='register'),
        url(r'^doRegister/', views.doRegister, name='doRegister'),
        url(r'^komunikat/(?P<typ>\w+)/(?P<message>\w+)/$', views.komunikat, name='komunikat'),
        url(r'^filter/', views.filter, name='filter'),
        url(r'^doFilter/', views.doFilter, name='doFilter'),
        url(r'^showuserposts/(?P<user>\w+)/', views.showUserPosts, name='showUserPosts'),
)