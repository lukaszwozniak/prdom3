#from django.db import models
from google.appengine.ext import db as models

from django.utils import timezone

class Wpis(models.Model):
    autor = models.StringProperty()
    tresc = models.StringProperty()
    data = models.DateTimeProperty(auto_now_add = 1)

    def __unicode__(self):
        return unicode.format(u'{0} {1}: {2}', self.data, self.autor, self.tresc)

class User(models.Model):
    login = models.StringProperty()
    haslo = models.StringProperty()
    loggedin = models.BooleanProperty()

    def __unicode__(self):
        return unicode.format(u'{0} {1}', self.login, self.haslo)


def loginuser(login, haslo):
    u = User.all().filter('login = ', login).filter('haslo = ', haslo)
    if not u:
        return False
    else:
        u.loggedin = True
        return True

def logoutuser(login):
    u = User.all().filter('login = ', login)
    u.loggedin = False

def is_authenticated(login):
    u = User.all().filter('login = ', login)
    return u.loggedin
